package com.kshrd.homework2.controller;

import com.kshrd.homework2.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {

    private StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("studentList",studentService.getAll());
        return "index";
    }

    @GetMapping("/addNew")
    public String indexAdd(ModelMap modelMap) {
        modelMap.addAttribute("studentList",studentService.getAll());
        return "addNew";
    }

    @GetMapping("/viewPage")
    public String indexView(ModelMap modelMap) {
        modelMap.addAttribute("studentList",studentService.getAll());
        return "viewPage";
    }
}
