package com.kshrd.homework2.service.imp;

import com.kshrd.homework2.model.Student;
import com.kshrd.homework2.repository.StudentRepository;
import com.kshrd.homework2.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    private StudentRepository studentRepository;

    @Autowired
    public StudentServiceImp(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.getAllStudent();
    }
}
