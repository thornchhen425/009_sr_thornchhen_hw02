package com.kshrd.homework2.service;

import com.kshrd.homework2.model.Student;

import java.util.List;

public interface StudentService {

    public List<Student> getAll();
}