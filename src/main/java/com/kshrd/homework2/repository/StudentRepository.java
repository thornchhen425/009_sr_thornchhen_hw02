package com.kshrd.homework2.repository;

import com.kshrd.homework2.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student> studentList = new ArrayList<>();
    public StudentRepository() {
       for (int i=1; i<=10; i++) {
           Student student = new Student();
           student.setId(i);
           student.setTitle("KSRHD");
           student.setDesc("I love KSHRD");
           studentList.add(student);
       }
    }
//    public boolean AddStudent() {
//        Student student = new Student();
//        for (int i=1; i<=10; i++) {
//           student.setId(i);
//           student.setTitle("KSRHD");
//           student.setDesc("I love KDHRD");
//           studentList.add(student);
//       }
//        return true;
//    }
    public List<Student> getAllStudent() {
        return studentList;
    }

}
